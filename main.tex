\documentclass[12pt]{article}
\usepackage[letterpaper]{geometry}
\usepackage{graphicx}
\usepackage{enumitem}
\usepackage{flafter}
\usepackage{float}
\usepackage{longtable}
\usepackage{multicol}
\usepackage{listings}
\usepackage{mathptmx}
\usepackage{caption}
\setlist[itemize]{itemsep=0mm}
\usepackage[font=small,labelfont=bf]{caption}
\usepackage[capitalise]{cleveref}
\geometry{top=1.0in, bottom=1.0in, left=1.0in, right=1.0in}
\usepackage{setspace}
\usepackage{afterpage}
\usepackage[authoryear,round,longnamesfirst]{natbib}

\setlength\headsep{0.333in}
\bibliographystyle{oas}
\setcitestyle{aysep={},comma}

\AtBeginDocument{%
    \crefname{table}{table}{tables}%
    \crefname{figure}{fig.}{fig.}
    \Crefname{figure}{Fig.}{Fig.}
}

%%%% TITLE %%%%
\title{Drowning Detection through Machine Learning and its Applications}

%%%% AUTHOR + DATE %%%%
\author{
    Elena Arnold \and
    Souvik Banerjee \and
    Sean Geiger \and
    Calvin Ly \and
    Evonne Ng \and
    Nolan Daniels
}
\date{\today}

\begin{document}
    \maketitle
    \begin{abstract}
        Lorem Ipsum is a dummy text that is mainly used by the printing and design industry. 
        It is intended to show how the type will look before the end product is available.
        
        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
    \end{abstract}
    \section{Introduction}
    %%% How to do citations %%%
    
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat \citep{Souvik-S3-9783319093956}. 
    Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur \citep{Souvik-S2-9780387307718}. 
    Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum \citep{Souvik-S1-6907221}.
    
    \section{Objective}
    
    \begin{enumerate}
      \item Install cameras in public swimming pools and collect data for machine learning
      \item Develop an algorithm for training the recurrent neural network and determine the key characteristics of drowning that can be recognized by a computer
      \item Implement algorithm and trained neural network and test its effectiveness in the real world
    \end{enumerate}
    
    \section{Methods}
    The main focus of our research will be analyzing the accuracy and performance of a the drowning detection system, with specific attention to determining the best heuristics to detect people who are drowning and finding the optimal topology and weights of the recurrent neural network.
    \begin{itemize}
      \item Heuristics:
      
      Object detection algorithms and frameworks already exist. The most widely used framework for object detection is the Viola-Jones object detection framework \citep{Souvik-S4-6269796}. This framework was proposed in 2001 by Michael Jones and Paul Viola and is most commonly used for face detection. However, the learning algorithm for the Viola-Jones object detection framework is not specific to face detection and can easily be repurposed for detecting people in a body of water. In addition, the Viola-Jones learning algorithm uses a variant of AdaBoost to improve learning performance. AdaBoost is a machine-learning algorithm that uses many weak but fast learning learning algorithms to generate ``boosted'' classifier. AdaBoost has been shown to be effect for both face detection and human detection \citep{Souvik-S5-6255762}.
      
      Our work in developing heuristics for detecting people in aquatic environments will build upon the work of \cite{Souvik-S5-6255762}, who improved upon the work of Viola and Jones by using local-binary patterns. Local-binary patterns are used in conjunction with support vector machines to detect textures. We plan to extend this research to detecting humans in water. Some of the issues we will have to solve are sunlight reflection and refraction, which distort the image. Some of the methods we will use are color correction and use of technology such as sonar to augment visual data.
      
      \item Neural network topology and choosing weights:
      
      Choosing the number of layers in the neural network and the number of nodes in each layer is dependent on the use case of the neural network.
      For example, classifying linearly separable data with a neural network requires no hidden layer neurons. We do not believe that identifying drowning is a problem involving linearly separable data since so we will need hidden layers. Determining how many hidden layers and the number of nodes in each hidden layer is the issue we have to solve. There are some vague rules of thumb researchers currently use, such as $H = O + (0.75I)$ where $H < 2I$ ($H$ is the number of hidden neurons, $O$ is the number of output neurons, and $I$ is the number of input neurons) \citep{Souvik-S6-Shahamiri2014199}. One possible method of identifying the optimal neural network topology is to use genetic algorithms such as NeuroEvolution of Augmenting Topologies (NEAT) \citep{Souvik-S7-NEAT}. NEAT uses simulated genetic mutations and recombinations to evolve neural network topologies using a fitness function, typically defined as the accuracy of the neural network when classifying data. Algorithms like NEAT can also modify synapse weights \citep{Souvik-S7-NEAT}. \Cref{fig:NEAT} shows how neural network topologies and weights can evolve using NEAT.
      
      \afterpage{
      \begin{figure}[htp!]
        \centering
        \includegraphics[width=0.7\textwidth]{images/iterative_ES_HyperNEAT.jpg}
        \caption{Evolution of a neural network over time using NEAT \citep{Souvik-S8-Risi:2011}}
        \label{fig:NEAT}
      \end{figure}
      \clearpage}
      
      
    \end{itemize}
    \section{Expected Outcomes}
    The data retrieved from local pools and analyzed through artificial intelligence will significantly increase societal understanding of human distress and warning signals. In particular, the research will target visual representations of drowning and identify which cues indicate someone is in need of assistance. By placing cameras above, around, and under the pool, the computer will be able to monitor the entire human body. While it is expected for different humans to react differently in emergency situations, it is nevertheless, very likely, that our research will reveal certain characteristic similarities in displaying distress. It is hypothesized that significant increases in kicking and hand waving can allude to drowning. 
    
	The data collected from this research will therefore focus primarily on bodily motion and physical responses. However, in order to accurately identify drowning incidents, the data examined must contain both positive controls documenting people actually drowning, and negative controls accounting for people horseplaying in the waters. The goal is to collect a variety of human reactions documenting not only instances of drowning, but also instances of horseplay that include people mimicking the actions of drowning. Samples from either conditions should indicate similarities in both scenarios, but more importantly, should highlight discrepancies in regular and abnormal activities. From the data collected and analysis performed with artificial intelligence, our research will lead to automated lifeguards which will be able to detect signs of drowning more accurately and more immediately than human lifeguards are capable of doing. 
    
	Still, the data collected from our research is expected to reach further than simply detecting and reacting towards distress. Since it is anticipated that in reality, not all instances of distress result in drowning, our live feed should be able to collect scenarios in which a person resurfaces. In such cases, it is preferable for robot to give the human space to recover by themselves. Therefore, our data will also analyze these situations in a realistic pool setting, differentiating them from actual drowning incidents. Ideally, the programmed robot should be deployed once a distress signal is detected. Upon reaching its destination, however, the robot should perform further analysis to conclude whether or not action is necessary. If the victim has recovered, the robot should return. 
	
    Comprehensively, the data collected across a variety of public pools will be able to define distress signals indicating drowning. The expected outcome is to be able to isolate characteristic signals of drowning that will warrant a preemptive prevention tactic. However, the knowledge gained from this research reaches far beyond the scope of public pools. By analyzing human behavior in an unsimulated environment, the aim is to be able to develop a formula to classify physical cues, differentiating between actions that demand immediate assistance and those that do not.  

    \bibliography{references.bib}
\end{document}
